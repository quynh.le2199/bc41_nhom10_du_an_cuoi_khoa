export const dataService = [
    {
        id : 1 , 
        content : 'Minimalist logo design'
    } , 
    {
        id : 2 , 
        content : 'Signature logo design'
    } , 
    {
        id : 3 , 
        content : 'Mascot logo design'
    } , 
    {
        id : 4 , 
        content : '3d logo design'
    } , 
    {
        id : 5 , 
        content : 'Hand drawn logo design'
    } , 
    {
        id : 6 , 
        content : 'Vintage logo design'
    } , 
    {
        id : 7 , 
        content : 'Remove background'
    } , 
    {
        id : 8 , 
        content : 'Photo restoration'
    } , 
    {
        id : 9 , 
        content : 'Photo retouching'
    } , 
    {
        id : 10 , 
        content : 'Image resize'
    } , 
    {
        id : 11, 
        content : 'Product label design'
    } , 
    {
        id : 12 , 
        content : 'Custom twitch overlay'
    } , 
    {
        id : 13 , 
        content : 'Custom twitch emotes'
    } , 
    {
        id : 14 , 
        content : 'Gaming logo'
    } , 
    {
        id : 15 , 
        content : 'Children book illustration'
    } 
]