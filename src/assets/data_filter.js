const dataFilter = [
    {
        id : 1 , 
        content : 'Category'
    } , 
    {
        id : 2 , 
        content : 'Service Options'
    } , 
    {
        id : 3 , 
        content : 'Seller Details'
    } , 
    {
        id : 4 , 
        content : 'Delivery Time'
    }
]

const dataSwitch = [
    {
        id : 1 , 
        content : 'Pro service'
    } , 
    {
        id : 2 , 
        content : 'Local sellers'
    } , 
    {
        id : 3 , 
        content : 'Online sellers'
    }
]

export {
    dataFilter , 
    dataSwitch
}