import React, { useEffect, useState } from 'react'
import ListJob from './ListJob/ListJob'
import { useDispatch, useSelector } from 'react-redux'
import { setHomePage } from '../../Toolkits/HomeSlice';
import { useParams } from 'react-router-dom';
import { setLoadingOff, setLoadingOn } from '../../Toolkits/spinnerSlice';
import { workServ } from '../../Services/WorkService';
import FilterBar from './FilterBar/FilterBar';

export default function ResultPage() {
    const [jobList, setJobList] = useState([])
    let param = useParams();
    let dispatch = useDispatch();
    useEffect(() => {
        dispatch(setHomePage())
    }, [])
    useEffect(() => {
        dispatch(setLoadingOn())
        workServ.getJobListByName(param.id)
            .then((res) => {
                dispatch(setLoadingOff());
                setJobList(res.data.content);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    }, [param.id])
    return (
        <div className='md:mt-28 mt-40 border-t border-gray-200'>
            <FilterBar keyWord={param.id} numOfJob={jobList.length} />
            <ListJob data={jobList} />
        </div>
    )
}
