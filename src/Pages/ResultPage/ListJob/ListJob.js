import React, { useEffect, useState } from 'react'
import { workServ } from '../../../Services/WorkService'
import { useDispatch, useSelector } from 'react-redux'
import { setLoadingOff, setLoadingOn } from '../../../Toolkits/spinnerSlice';
import ItemJob from './ItemJob';

export default function ListJob({ data }) {
    return (
        <div id='listJob' className='container'>
            <div className=' mx-auto xxl:w-11/12 row'>
                {data.map((item) => {
                    return <ItemJob data={item} key={item.id} />
                })}
            </div>
        </div>
    )
}
