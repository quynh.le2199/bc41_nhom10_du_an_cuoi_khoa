import React, { useEffect , useState } from 'react'
import { useDispatch } from 'react-redux'
import Header from '../../Components/Header/Header';
import CategoriesMenu from '../../Components/CategoriesMenu/CategoriesMenu';
import { setHomePage } from '../../Toolkits/HomeSlice';
import { NavLink, useParams } from 'react-router-dom';
import { workServ } from '../../Services/WorkService';
import '../../css/titlePage.css' ; 
import { data_titlepage } from '../../assets/data_titlepage';
import Footer from '../../Components/Footer/Footer';
import { dataService } from '../../assets/data_service';

function TitlePage() {
    const [listTypeWork, setListTypeWork] = useState({}) ; 
    const [listDetailTypeWork, setListDetailTypeWork] = useState([]) ; 
    let dispatch = useDispatch() ; 
    dispatch(setHomePage()) ;
    let params = useParams() ; 
    let id = params.id ; 
    let fetchDetailedWork = async() => {
        let res = await workServ.getDetailTypeWork(id) ; 
        console.log(res.data.content[0]);
        setListTypeWork(res.data.content[0]) ; 
        setListDetailTypeWork(res.data.content[0].dsNhomChiTietLoai) ; 

    }
    useEffect(() => {
        fetchDetailedWork() ; 
    }, [id]) ; 
    
    return (
        <>
            <Header/>
            <CategoriesMenu/>
            <div className="banner-job-title bg-green-900">
                <div className="content h-full text-white flex justify-center items-center flex-col space-y-5">
                    <h1 className='text-3xl font-bold'>{listTypeWork.tenLoaiCongViec}</h1>
                    <p className='text-xl'>Designs to make you stand out.</p>
                    <button className='button__banner-job-title border p-3 rounded bg-transparent'><i className="fa fa-play-circle" aria-hidden="true"></i> How Fiverr Works</button>
                </div>
            </div>
            <div className="popular-job-title p-5">
                <div className="container">
                    <h1 className='text-2xl text-black font-bold mb-3'>
                        Most popular in {listTypeWork.tenLoaiCongViec}
                    </h1>
                    <div className="content grid lg:grid-cols-5 md:grid-cols-3 gap-3">
                        {data_titlepage.map((item , index) => {
                            return (
                                <div key={index} className='content__item p-3 bg-white shadow-sm flex items-center rounded'>
                                    <img className='w-8 h-8' src={item.image} alt="image_title" />
                                    <p className='text-black font-bold text_content'>{item.title} <i className="fa fa-arrow-right ml-1" aria-hidden="true"></i></p>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
            <div className="explore-job-title">
                <div className="container">
                    <h1 className='text-2xl text-gray-700'>Explore {listTypeWork.tenLoaiCongViec}</h1>
                    <div className='content flex'>
                        {listDetailTypeWork.map((item , index) => {
                            return (
                                <div key={index} className='mr-5'>
                                    <img src={item.hinhAnh} className='w-72' alt="image_detail" />
                                    <h2 className='text-xl mb-6'>{item.tenNhom}</h2>
                                    <ul>
                                        {item.dsChiTietLoai.map((work , index) => {
                                            return (
                                                <NavLink key={index} to={`/categories/${work.id}`}>
                                                    <li className='mb-3 font-medium '>{work.tenChiTiet}</li>
                                                </NavLink>
                                            )
                                        })}
                                    </ul>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
            <div className="service-job-title py-10">
                <div className="container">
                    <h1 className='text-2xl font-bold text-gray-700 text-center'>Services Related To {listTypeWork.tenLoaiCongViec}</h1>
                    <div className="content flex flex-wrap items-center justify-center">
                        {dataService.map((item) => {
                            return <span className='bg-gray-100 p-2 rounded-md font-bold m-2 text-gray-600' key={item.id}>{item.content}</span>
                        })}
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    )
}

export default TitlePage
