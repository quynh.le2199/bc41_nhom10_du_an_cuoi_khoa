import React from 'react'
import OwlCarousel from 'react-owl-carousel';
import { dataServices } from '../../../assets/data_services';

export default function ServicesTablet() {
    let renderContent = () => {
        return dataServices.map((item) => {
            return (
                <div key={item.id} className='item'>
                    <img src={item.img} alt="" className='' />
                </div>
            )
        })
    }
    return (
        <div id='sevices' className='servicesTablet'>
            <div className="trustedBy">
                <div className="container flex justify-center items-center space-x-8">
                    <span>Trusted by: </span>
                    <ul className='flex justify-center items-center space-x-4'>
                        <li><img src="./images/fb.png" alt="" className='w-20' /></li>
                        <li><img src="./images/google.png" alt="" className='w-20' /></li>
                        <li><img src="./images/netflix.png" alt="" className='w-20' /></li>
                        <li><img src="./images/pg.png" alt="" className='w-20' /></li>
                        <li><img src="./images/paypal.png" alt="" className='w-20' /></li>
                    </ul>
                </div>
            </div>
            <div className='carousel '>
                <div className="container">
                    <div className='mx-auto xxl:w-11/12 pt-16 mb-3'>
                        <h2 className='mb-4'>Popular professional services</h2>
                        <OwlCarousel className='owl-theme' loop nav dots={false} items={2} stagePadding={20} margin={18} >
                            {renderContent()}
                        </OwlCarousel>
                    </div>
                </div>
            </div>
        </div>
    )
}
