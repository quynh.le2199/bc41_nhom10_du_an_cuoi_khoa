import React from 'react'
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { dataModal } from '../../../assets/data_modal';
import { useDispatch } from 'react-redux';
import { handleOpenModal } from '../../../Toolkits/modalSlice';
import ModalPopUp from '../../../Components/Modal/Modal';

export default function TestimonialDesktopToBelow() {
  const disptach = useDispatch();
  let handleShowModal = (modalId) => {
    disptach(handleOpenModal(modalId));
  }
  let renderThumbnail = () => {
    return dataModal.map((item, index) => {
      return (
        <div key={index} >
          <div className='item w-9/12 mx-auto'>
            <button onClick={() => { handleShowModal(item.media) }} className='item__button w-full'>
              <img src={item.thumbnail} alt="" />
            </button>
          </div>
        </div>
      )
    })
  }
  return (
    <div id='testimonial' className='testimonialDesktopToBelow'>
      <div className="container">
        <div className='mx-auto xl:w-11/12 pt-4 mb-10'>
          <OwlCarousel className='owl-theme' loop nav dots={false} items={1} stagePadding={50} margin={55} >
            {renderThumbnail()}
          </OwlCarousel>
        </div>
      </div>
      <ModalPopUp />
    </div>
  )
}
