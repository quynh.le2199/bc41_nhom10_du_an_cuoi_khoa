import React, { useEffect, useState } from 'react'
import { adminServ } from '../../../Services/adminService';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../Toolkits/spinnerSlice';
import { Button, Table, Tooltip, message, Modal } from 'antd';
import { headerColumns } from '../utils';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import '../../../css/userTable.css'
import { showEditModal } from '../../../Toolkits/adminSlice';

export default function ServicesTable() {
    const [servicesList, setServicesList] = useState([])
    let dispatch = useDispatch();
    useEffect(() => {
        fetchServicesList()
    }, [])
    let fetchServicesList = () => {
        dispatch(setLoadingOn());
        adminServ.getServicesList()
            .then((res) => {
                dispatch(setLoadingOff());
                let list = res.data.content.map((service) => {
                    return {
                        ...service, action: (
                            <div className='space-x-2 flex items-center '>
                                <Tooltip title="Edit">
                                    <Button onClick={() => { handleEditService(service) }} shape="circle" icon={<EditOutlined />} className='edit flex items-center justify-center' />
                                </Tooltip>
                                <Tooltip title="Delete">
                                    <Button onClick={() => { showConfirm(service.id) }} shape="circle" icon={<DeleteOutlined />} className='delete flex items-center justify-center' />
                                </Tooltip>
                            </div>
                        )
                    }
                })
                setServicesList(list);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    }
    let handleDeleteService = (serviceId) => {
        dispatch(setLoadingOn())
        adminServ.deleteService(serviceId)
            .then((res) => {
                dispatch(setLoadingOff());
                message.success("Xóa dịch vụ thành công");
                fetchServicesList();
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                message.error("Đã có lỗi xảy ra");
                console.log(err);
            });
    }
    let handleEditService = (service) => {
        dispatch(showEditModal(service));
    }

    const { confirm } = Modal;
    const showConfirm = (serviceId) => {
        confirm({
            icon: '',
            content: (
                <div className='space-y-3'>
                    <h1>Xác nhận xóa dịch vụ này</h1>
                </div>
            ),
            onOk() {
                handleDeleteService(serviceId);
            },
            onCancel() { }
        });
    };

    return (
        <div id='adminTable' className='p-3 bg-white mt-3'>
            <Table dataSource={servicesList} columns={headerColumns} />
        </div>
    );
};
