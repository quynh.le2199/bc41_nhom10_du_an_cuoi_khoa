import React from "react";
import { Button, Checkbox, Form, Input, Radio, message } from "antd";
import { userSer } from "../../Services/userService";
import { useNavigate } from "react-router";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../Services/localService";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../Toolkits/userSlice";
import { setHomePage } from '../../Toolkits/HomeSlice';

export default function LoginPage() {
    const [form] = Form.useForm();
    const navigate = useNavigate()
    const dispatch = useDispatch()
    dispatch(setHomePage());
    let navigatePage = () => {
        let userRole = localUserServ.get();
        if (userRole.user.role !== 'ADMIN') {
            navigate("/profile");
        }
        else {
            navigate("/admin-users");
        }
    }
    const onFinish = (values) => {
        userSer
            .postSingin(values)
            .then((res) => {
                localUserServ.set(res.data.content)
                dispatch(setUserInfo(res.data.content))
                message.success("Đăng nhập thành công !")
                navigatePage();
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
                message.error("Đăng nhập thất bại !")
            });
    };
    return (
        <div style={{ width: "900px", boxShadow: "0 15px 16.83px 0.17px rgba(0,0,0,.05)", border: "1px solid rgba(0,0,0,.05)", marginTop: '150px', marginBottom: '48px' }} className="container mx-auto rounded-2xl mb-14 mt-10">
            <div className="flex flex-wrap-reverse pb-20 pt-16">
                <div className="p-7 login__left">
                    <img className="w-full" src="https://demo5.cybersoft.edu.vn/static/media/signin.6f1c72291c1ec0817ded.jpg" alt="" />
                </div>
                <div className="login__right pl-9 pr-3">
                    <h2 className="mb-12 font-bold ml-12 text-3xl text-green-500">Signin to Fiver</h2>

                    <Form
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{
                            residence: ["zhejiang", "hangzhou", "xihu"],
                            prefix: "86",
                        }}
                        className="w-full "
                        scrollToFirstError
                        layout="vertical"
                        wrapperCol={24}
                        labelCol={24}
                        autoComplete="off"
                    >


                        <Form.Item
                            name="email"
                            rules={[
                                {
                                    type: "email",
                                    message: "Chưa đúng định dạng email !",
                                },
                                {
                                    required: true,
                                    message: "Vui lòng nhập email của bạn !",
                                },
                            ]}
                        >
                            <Input className="text-lg font-normal" placeholder="Your email" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: "Vui lòng nhập mập khẩu!",
                                },
                                {
                                    min: 8,
                                    message: "Tối thiểu 8 kí tự",
                                },
                            ]}
                        >
                            <Input.Password
                                className="text-lg font-normal"
                                placeholder="Your password"
                            />
                        </Form.Item>



                        <Form.Item className="py-4 px-3  ">
                            <Button

                                className="   bg-green-500 hover:text-green-500 text-white"
                                htmlType="submit"
                            >
                                Login
                            </Button>
                            <NavLink style={{ textDecoration: "none" }} className="no-underline" to="/register"> <a style={{ textDecoration: "none" }} className="text-sm text-green-400 hover:text-blue-400 no-underline" href="">Register now ?</a></NavLink>
                        </Form.Item>
                    </Form>
                </div>

            </div>
        </div>
    );
}

