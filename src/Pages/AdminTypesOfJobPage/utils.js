import { Tag } from "antd"

export const headerColumns = [
    {
        title: 'Mã loại công việc',
        dataIndex: 'id',
        key: 'id',
        render: (id) => {
            switch (id) {
                case 1:
                    return <Tag className="font-medium" color="green">{id}</Tag>
                    break;
                case 2:
                    return <Tag className="font-medium" color="volcano">{id}</Tag>
                    break;
                case 3:
                    return <Tag className="font-medium" color="blue">{id}</Tag>
                    break;
                case 4:
                    return <Tag className="font-medium" color="magenta">{id}</Tag>
                    break;
                case 5:
                    return <Tag className="font-medium" color="purple">{id}</Tag>
                    break;

                default:
                    return <Tag className="font-medium" color="cyan">{id}</Tag>
                    break;
            }
        }
    },
    {
        title: 'Tên loại công việc',
        dataIndex: 'tenLoaiCongViec',
        key: 'tenLoaiCongViec',
        // width: '250px',

    },
    {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
    },
]
