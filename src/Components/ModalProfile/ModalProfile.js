import { Button, Form, Input, Modal, Radio, message } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userSer } from "../../Services/userService";
import { setUserInfo } from "../../Toolkits/userSlice";
export default function ModalProfile() {
    const [form] = Form.useForm();
    const { user } = useSelector((state) => state.userSlice.userInfo);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const dispatch = useDispatch()
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
        onFinish()
    };
    const handleCancel = () => {
        setIsModalOpen(false);
        window.location.reload();
    };
    const onFinish = (values) => {
        userSer.putUser(user.id, values).then((res) => {
            dispatch(setUserInfo(res.data.content))
            message.success("Cập nhật thông tin thành công !")
            window.location.reload();
            console.log(res);
        })
            .catch((err) => {
                console.log(err);
            });

    };
    return (
        <div>
            <Button
                className="border-none text-gray-400 text-base hover:text-inherit"
                onClick={showModal}
            >
                <i class="fa fa-pen"></i>
            </Button>
            <Modal
                title="Update User"
                open={isModalOpen}
                onCancel={handleCancel}
                className="text-center text-3xl"

            >
                <div className="">
                    <Form
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{
                            residence: ["zhejiang", "hangzhou", "xihu"],
                            prefix: "86",
                        }}
                        className="w-full grid grid-cols-2 gap-2"
                        scrollToFirstError
                        layout="vertical"
                        wrapperCol={24}
                        labelCol={24}
                        autoComplete="off"
                    >
                        <Form.Item initialValue={user ? user.email : null} label="Email" name="email">
                            <Input className="text-lg font-normal" />
                        </Form.Item>
                        <Form.Item initialValue={user ? user.phone : null} label="Phone" name="phone">
                            <Input className="text-lg font-normal" />
                        </Form.Item>
                        <Form.Item initialValue={user ? user.name : null} label="Name" name="name">
                            <Input className="text-lg font-normal" placeholder="Your name" />
                        </Form.Item>
                        <Form.Item initialValue={user ? user.birthday : null} label="Birthday" name="birthday">
                            <Input className="text-lg font-normal" placeholder="Your email" />
                        </Form.Item>
                        <Form.Item initialValue={user ? user.gender : null} className="block" label="Gender">
                            <Radio.Group>
                                <Radio className="text-2xl" value="male">
                                    {" "}
                                    Male{" "}
                                </Radio>
                                <Radio value="female"> Female </Radio>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item name="cetification">

                        </Form.Item>
                        <Form.Item name="cetification">
                            <Input className="text-lg font-normal" placeholder="Your cetification" />
                        </Form.Item>
                        <Form.Item name="skill">
                            <Input className="text-lg font-normal" placeholder="Your skill" />
                        </Form.Item>
                        <Form.Item>
                            <Button
                                type="primary"
                                className="text-center  font-medium bg-green-500 "
                                htmlType="submit"
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </Modal>
        </div>
    );
}
