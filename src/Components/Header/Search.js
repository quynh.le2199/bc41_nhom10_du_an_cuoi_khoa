import React from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux';
import { getFormValue } from '../../Toolkits/formSlice';
import { useNavigate } from 'react-router-dom';

function Search() {
    let { isHomePage1 } = useSelector(state => state.HomeSlice);
    let { formVal } = useSelector(state => state.formSlice)
    let dispatch = useDispatch();
    let navigate = useNavigate();

    let renderActive = () => {
        if (isHomePage1) {
            return 'search__service'
        } else {
            return 'search__service search__service-active'
        }
    }
    let handleSubmit = (value) => {
        navigate(`/result/${value}`);
    }
    let handleOnchange = (e) => {
        dispatch(getFormValue(e.target.value));
    }
    return (
        <>
            <Form id='search__service' className={renderActive()} onSubmit={() => { handleSubmit(formVal) }}>
                <Form.Control
                    type="search"
                    placeholder="Find Service"
                    className='ml-10 mr-2 search__input'
                    aria-label="Search"
                    onChange={handleOnchange}
                />
                <Button variant="success" onClick={() => { handleSubmit(formVal) }}>Search</Button>
            </Form>
        </>
    )
}

export default Search