import { https } from "./config"

export const workServ = {
    getMenuTypeWork: () => {
        return https.get('/api/cong-viec/lay-menu-loai-cong-viec')
    },
    getJobListByName: (id) => {
        return https.get(`/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${id}`)
    } , 
    getDetailTypeWork : (id) => {
        return https.get(`/api/cong-viec/lay-chi-tiet-loai-cong-viec/${id}`) 
    } , 
    getJobByType : (id) => {
        return https.get(`/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${id}`)
    } , 
    getCommentJob : (id) => {
        return https.get(`/api/binh-luan/lay-binh-luan-theo-cong-viec/${id}`)
    } , 
    getDetailedJob : (id) => {
        return https.get(`/api/cong-viec/lay-cong-viec-chi-tiet/${id}`)
    } , 
    postComment : (data) => {
        return https.post(`api/binh-luan` , data)
    }
}