import axios from "axios"
import { localUserServ } from "./localService";

const baseURL = 'https://fiverrnew.cybersoft.edu.vn';
const TokenCyberSoft = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q';
// const tokenUser = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI1MzEiLCJlbWFpbCI6InZpdmlhbkBnbWFpbC5jb20iLCJyb2xlIjoiQURNSU4iLCJuYmYiOjE2ODQ5MTg5NjksImV4cCI6MTY4NTUyMzc2OX0.d7cYYgkOXf-H6ByV7ALoFsv8ag7HtiAUu5ZKTqFDS8o'
// const tokenUser = localUserServ.get();

export const configHeader = () => {
    return {
        // * Nơi chứa token học viên 
        TokenCyberSoft: TokenCyberSoft,
        token:  localUserServ.get()?localUserServ.get().token : '' ,
        // * nơi chứa token access của acount (tạo ra sau khi đăng nhập tài khoản thành công)
        // token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI0NDgiLCJlbWFpbCI6ImR1eWx0c2ExNjAxMDlAZnB0LmVkdS52biIsInJvbGUiOiJVU0VSIiwibmJmIjoxNjg0ODU1Mjg5LCJleHAiOjE2ODU0NjAwODl9.dOoL8Fu1EUiBG0LaubaGNH_sifoB-5JMzxwQZ-tAjO0'
    }
}

export const https = axios.create({
    baseURL: baseURL,
    headers: configHeader(),
})